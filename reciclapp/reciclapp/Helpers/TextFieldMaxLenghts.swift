//
//  TextFieldMaxLenghts.swift
//  reciclapp
//
//  Created by Juan Nicolas Galvis Ortiz on 10/15/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import Foundation
import UIKit

private var maxLengths = [UITextField: Int] ()

extension UITextField{
    
    @IBInspectable var maxLength: Int {
        get {
            guard let length = maxLengths[self] else{
                return Int.max
            }
            return length
        }
        set{
            maxLengths[self] = newValue
            
             addTarget(
                   self,
                   action: #selector(limitLength),
                   for: UIControl.Event.editingChanged
                 )
            
        }
    }
    @objc func limitLength(textField: UITextField) {
       // 6
       guard let prospectiveText = textField.text,
                 prospectiveText.count > maxLength
       else {
         return
       }
       
       let selection = selectedTextRange
       // 7
       let maxCharIndex = prospectiveText.index(prospectiveText.startIndex, offsetBy: maxLength)
       text = prospectiveText.substring(to: maxCharIndex)
       selectedTextRange = selection
     }
}
