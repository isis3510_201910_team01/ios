//
//  LoginViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/4/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit
import Firebase
//import GoogleSignIn

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet weak var gmailBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    
    
    override func viewDidLoad() {
         let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(fire), userInfo: nil, repeats: true)
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
//        Login Firebase
        // Do any additional setup after loading the view.
        //gmailBtn.sharedInstance().presentingViewController = self
        
        setupElements()
    }
    func setupElements(){
        // Dissapear error label
        errorLabel.alpha=0
    }
    @objc func fire()
    {
        if Connection.isConnected() == false{
            let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func loginTapped(_ sender: Any) {
        //Signs in the user in Firebase
        guard
          let email = emailTextField.text,
          let password = passwordTextField.text,
          
          email.count > 0,
          password.count > 0
          else {
            return
        }
        UserDefaults.standard.set(email, forKey: "email")
        UserDefaults.standard.set(0, forKey: "puntos")
        if Connection.isConnected() == false{
            let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
            print ("mandando alert")
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
        }
        if Connection.isConnected() == true{

        Auth.auth().signIn(withEmail: email, password: password) { user, error in
          if let error = error, user == nil {
            let alert = UIAlertController(title: "El ingreso falló",
                                          message: "Verifique conexión a internet",
                                          preferredStyle: .alert)
            self.showError(mensaje: "El ingreso falló")
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
          }
          else{
             self.goToHome()
            }
        }
        print("hizo login")
        }
        else {
                   let alert = UIAlertController(title: "Error", message: "Verifique su conexión a internet", preferredStyle: .alert)
                   
                   alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                   self.present(alert, animated: true)
               }
        //Sends to the Home Screen
       
    }
    
    func showError(mensaje : String){
        errorLabel.text = mensaje
        errorLabel.alpha = 1
    }
//     // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
//         self.transicionAlHome()
    }
    
   func goToHome(){
   print ("Rumbo a home")
        let homeViewController = storyboard?.instantiateViewController (withIdentifier: "tabbar") as! UITabBarController
   
    
        view.window?.rootViewController = homeViewController
        view.window?.makeKeyAndVisible()
    print ("Lo logramos")
    
    
    }

 
    @IBAction func newTapped(_ sender: Any) {
        let homeViewController = storyboard?.instantiateViewController (withIdentifier: "RegistrationVC") as! UIViewController
        
         
             view.window?.rootViewController = homeViewController
             view.window?.makeKeyAndVisible()
         print ("Lo logramos")
        
    }
    
}
