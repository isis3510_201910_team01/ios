//
//  ProfileViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/7/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    @IBOutlet weak var userLabel: UILabel!
    
    @IBOutlet weak var levelLabel: UILabel!
    
    @IBOutlet weak var percentageLabel: UILabel!
    
    override func viewDidLoad() {
        let name = UserDefaults.standard.string(forKey: "email") ?? "Usuario"
            
         var points = UserDefaults.standard.integer(forKey: "puntos") ?? 0
         userLabel.text = name
        levelLabel.text = "Nivel " + "\(1 + (points/50))"
        percentageLabel.text = "\(100*(points/50))"+" %"
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
