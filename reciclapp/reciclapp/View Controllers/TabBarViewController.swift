//
//  TabBarViewController.swift
//  reciclapp
//
//  Created by Paola Latino on 10/7/19.
//  Copyright © 2019 Paola Latino. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController {

    @IBOutlet weak var mainTab: UITabBar!
    var conectado : Bool!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            self.handleButtons()
        }
        handleButtons();
        // Do any additional setup after loading the view.
    }

    func handleButtons(){
    DispatchQueue.global(qos: .background).async {
        if !Connection.isConnected(){
           if self.tabBar.items?[2].isEnabled == true{
                    DispatchQueue.main.async {
                        createAlertNetwork(title: "Error", message: "Verifique la conexión a internet" )
                    }
                }
            }
            if self.tabBar.items?[3].isEnabled == true{
                    DispatchQueue.main.async {
                        createAlertNetwork(title: "Error", message: "Verifique la conexión a internet" )
                    }
                }
            }
         if self.tabBar.items?[5].isEnabled == true{
                DispatchQueue.main.async {
                    createAlertNetwork(title: "Error", message: "Verifique la conexión a internet" )
                }
            }
        }
    }
    func createAlertNetwork(title: String, message: String ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default , handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
